Feature: DEV-0001 Managing owners

  Scenario: DEV-0001 : TC#1 - Owner flow
    Given I create owner:
      | id        | 0                  |
      | firstName | Vardenis           |
      | lastName  | Pavardenis         |
      | address   | Konstitutcijos pr. |
      | city      | Vilnius            |
      | telephone | 123456             |
    When I request all owners
    Then created owner is one of them

    When I change created owner name to "Jonas"
    And I request changed owner
    Then Owner has name "Jonas"

    When I delete created owner
    And I request all owners
    Then created owner is not one of them
