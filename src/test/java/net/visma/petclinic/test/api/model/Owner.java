package net.visma.petclinic.test.api.model;

import lombok.Data;

@Data
public class Owner {
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String telephone;
}
