package net.visma.petclinic.test.api.stepdefs;

import cucumber.api.Transform;
import cucumber.api.Transpose;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.visma.petclinic.test.api.model.Owner;
import net.visma.petclinic.test.api.service.OwnerService;
import net.visma.petclinic.test.api.utils.PositivePhraseTransformer;

import javax.inject.Inject;
import javax.inject.Singleton;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@Singleton
public class OwnerStepdefs {
    private final OwnerService ownerService;

    @Inject
    public OwnerStepdefs(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @Given("^I create owner:$")
    public void iCreateOwner(@Transpose List<Owner> ownerRequest) throws Throwable {
        assertThat(ownerRequest, hasSize(1));
        ownerService.createOwner(ownerRequest.get(0));
    }

    @When("^I request all owners$")
    public void iRequestAllOwners() throws Throwable {
        ownerService.requestAllOwners();
    }

    @Then("^created owner (is|is not) one of them$")
    public void createdOwnerIsOneOfThem(@Transform(PositivePhraseTransformer.class) boolean expected) throws Throwable {
        assertThat(ownerService.createdOwnerIsAmongAllOwners(), is(expected));
    }

    @When("^I change created owner name to \"([^\"]*)\"$")
    public void iChangeCreatedOwnerNameTo(String newName) throws Throwable {
        ownerService.changeCreatedOwnerName(newName);
    }

    @And("^I request changed owner$")
    public void iRequestChangedOwner() throws Throwable {
        ownerService.requestChangedOwner();
    }

    @Then("^Owner has name \"([^\"]*)\"$")
    public void ownerHasName(String expectedName) throws Throwable {
        assertThat(ownerService.getOwner().getFirstName(), is(expectedName));
    }

    @When("^I delete created owner$")
    public void iDeleteCreatedOwner() throws Throwable {
        ownerService.deleteCreatedOwner();
    }
}
