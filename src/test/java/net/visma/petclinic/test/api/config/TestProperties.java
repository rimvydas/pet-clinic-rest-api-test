package net.visma.petclinic.test.api.config;

import org.constretto.ConstrettoBuilder;
import org.constretto.ConstrettoConfiguration;
import org.constretto.model.ClassPathResource;

public class TestProperties {
    private static final ConstrettoConfiguration config = initConfiguration();

    public static final String APP_URL = config.evaluateToString("pet.clinic.url");

    private static ConstrettoConfiguration initConfiguration() {
        return new ConstrettoBuilder()
                .createPropertiesStore()
                .addResource(new ClassPathResource("config/config.properties"))
                .done()
                .getConfiguration();
    }
}
