package net.visma.petclinic.test.api;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        features = "src/test/resources/features/",
        glue = "net.visma.petclinic.test.api.stepdefs")
public class RunCukesTest {

}
