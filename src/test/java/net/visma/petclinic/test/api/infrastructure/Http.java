package net.visma.petclinic.test.api.infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Http {
    private String baseUrl;
    private final ObjectMapper objectMapper;

    public Http(String baseUrl, ObjectMapper objectMapper) {
        this.baseUrl = baseUrl;
        this.objectMapper = objectMapper;
    }

    public <T> T post(String url, Object body, Class<T> type) {
        RequestBuilder requestBuilder = commonRequest(HttpPost.METHOD_NAME, url);
        JsonDeserializer<T> deserializerByType = deserializeByType(type);
        return performRequest(postRequest(requestBuilder, body), deserializerByType);
    }

    public <T> T get(String url, TypeReference<T> type) {
        RequestBuilder requestBuilder = commonRequest(HttpGet.METHOD_NAME, url);
        JsonDeserializer<T> deserializerByReference = deserializeByReference(type);
        return performRequest(getRequest(requestBuilder), deserializerByReference);
    }

    public <T> T get(String url, Class<T> type) {
        RequestBuilder requestBuilder = commonRequest(HttpGet.METHOD_NAME, url);
        JsonDeserializer<T> deserializerByReference = deserializeByType(type);
        return performRequest(getRequest(requestBuilder), deserializerByReference);
    }

    public <T> T put(String url, Object body) {
        RequestBuilder requestBuilder = commonRequest(HttpPut.METHOD_NAME, url);
        return performRequest(postRequest(requestBuilder, body), null);
    }

    public void delete(String url) {
        RequestBuilder requestBuilder = commonRequest(HttpDelete.METHOD_NAME, url);
        performRequest(getRequest(requestBuilder), null);
    }

    private <T> T performRequest(HttpUriRequest httpRequest, JsonDeserializer<T> deserializer) {
        try {
            HttpClient httpClient = HttpClients.custom().build();
            HttpResponse response = httpClient.execute(httpRequest);

            validateResponse(response);

            if (deserializer == null)
            {
                return null;
            }

            String dataString = EntityUtils.toString(response.getEntity());
            return deserializer.fromJson(dataString);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void validateResponse(HttpResponse response) {
        if (!(response.getStatusLine().getStatusCode() / 100 == 2)) {
            throw new RuntimeException("Expected 2xx status, but was " + response.getStatusLine().getStatusCode());
        }
    }

    private HttpUriRequest getRequest(RequestBuilder requestBuilder) {
        return requestBuilder.build();
    }

    private HttpUriRequest postRequest(RequestBuilder requestBuilder, Object body) {
        return requestBuilder.setEntity(new StringEntity(toJson(body), ContentType.APPLICATION_JSON))
                .build();
    }

    private RequestBuilder commonRequest(String method, String url) {
        return RequestBuilder.create(method)
                .addHeader("Accept", "application/json")
                .setUri(baseUrl + url);
    }

    private String toJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private <T> JsonDeserializer<T> deserializeByType(Class<T> type) {
        return (s) -> {
            try {
                return objectMapper.readValue(s, type);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }

    private <T> JsonDeserializer<T> deserializeByReference(TypeReference<T> type) {
        return (s) -> {
            try {
                return objectMapper.readValue(s, type);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    interface JsonDeserializer<T> {
        T fromJson(String s);
    }
}
