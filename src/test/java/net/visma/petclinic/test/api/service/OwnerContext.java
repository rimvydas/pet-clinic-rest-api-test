package net.visma.petclinic.test.api.service;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.Setter;
import net.visma.petclinic.test.api.model.Owner;

import java.util.List;

@ScenarioScoped
@Getter
@Setter
public class OwnerContext {
    private Owner ownerRequest;

    private Owner ownerResponse;
    private List<Owner> owners;
}
