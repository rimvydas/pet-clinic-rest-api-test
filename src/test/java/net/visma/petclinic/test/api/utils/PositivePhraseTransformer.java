package net.visma.petclinic.test.api.utils;

import cucumber.api.Transformer;
import org.apache.commons.lang.StringUtils;

public class PositivePhraseTransformer extends Transformer<Boolean> {
    @Override
    public Boolean transform(String s) {
        return !StringUtils.containsIgnoreCase(s, "not");
    }
}
