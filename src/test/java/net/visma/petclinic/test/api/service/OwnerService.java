package net.visma.petclinic.test.api.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Objects;
import net.visma.petclinic.test.api.infrastructure.Http;
import net.visma.petclinic.test.api.model.Owner;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class OwnerService {
    private final Http http;
    private final Provider<OwnerContext> ownerContext;

    @Inject
    public OwnerService(Http http, Provider<OwnerContext> ownerContext) {
        this.http = http;
        this.ownerContext = ownerContext;
    }

    public void createOwner(Owner ownerRequest) {
        ownerContext.get().setOwnerRequest(ownerRequest);

        Owner response = http.post("/api/owners", ownerRequest, Owner.class);
        ownerContext.get().setOwnerResponse(response);
    }

    public void requestAllOwners() {
        List<Owner> response = http.get("/api/owners", new TypeReference<List<Owner>>() {
        });
        ownerContext.get().setOwners(response);
    }

    public boolean createdOwnerIsAmongAllOwners() {
        Owner createOwnerRequest = ownerContext.get().getOwnerRequest();

        for (Owner ownerResponse : ownerContext.get().getOwners()) {
            if (Objects.equal(ownerResponse.getFirstName(), createOwnerRequest.getFirstName())
                    && Objects.equal(ownerResponse.getLastName(), createOwnerRequest.getLastName())
                    && Objects.equal(ownerResponse.getAddress(), createOwnerRequest.getAddress())
                    && Objects.equal(ownerResponse.getCity(), createOwnerRequest.getCity())
                    && Objects.equal(ownerResponse.getTelephone(), createOwnerRequest.getTelephone())) {
                return true;
            }
        }

        return false;
    }

    public void changeCreatedOwnerName(String newName) {
        Owner createdOwner = ownerContext.get().getOwnerResponse();
        createdOwner.setFirstName(newName);

        http.put("/api/owners/" + createdOwner.getId(), createdOwner);
    }

    public void requestChangedOwner() {
        Owner changedOwner = ownerContext.get().getOwnerResponse();

        Owner ownerResponse = http.get("/api/owners/" + changedOwner.getId(), Owner.class);

        ownerContext.get().setOwnerResponse(ownerResponse);
    }

    public Owner getOwner() {
        return ownerContext.get().getOwnerResponse();
    }

    public void deleteCreatedOwner() {
        Owner createdOwner = ownerContext.get().getOwnerResponse();

        http.delete("/api/owners/" + createdOwner.getId());
    }
}
