package net.visma.petclinic.test.api.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import net.visma.petclinic.test.api.infrastructure.Http;

public class InjectorModule implements Module {

    @Override
    public void configure(Binder binder) {

    }

    @Provides
    @Singleton
    ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Provides
    @Singleton
    Http http(ObjectMapper objectMapper) {
        return new Http(TestProperties.APP_URL, objectMapper);
    }
}
