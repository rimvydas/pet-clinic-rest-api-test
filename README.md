To run tests:
- Start spring-petclinic-rest app from `https://github.com/spring-petclinic/spring-petclinic-rest`
- Execute command: `mvn clean test`
